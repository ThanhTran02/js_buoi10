function showMessage(idSpan, message) {
    document.getElementById(idSpan).innerText = message;
}
function kiemTraDoDai(min, max, idSpan, message, value) {
    var length = value.length;
    if (length <= max && length >= min) {
        showMessage(idSpan, "")
        return true;
    }
    else {
        showMessage(idSpan, message);
        return false;
    }
}
function kiemTraEmail(idSpan, email) {
    const re =
        /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (re.test(email)) {
        showMessage(idSpan, "")
        return true;
    }
    else {
        showMessage(idSpan, "email không hợp lệ")
        return false;
    }
}
function kiemtGiaTtriTrongKhoang(min, max, idSpan, message, number) {
    if (number <= max && number >= min) {
        showMessage(idSpan, "")
        return true;
    }
    else {
        showMessage(idSpan, message);
        return false;
    }
}
function checkName(idSpan, name) {
    var regName = /^[a-zA-Z]+ [a-zA-Z]+$/;
    if (!regName.test(name)) {
        showMessage(idSpan, 'Tên đưa ra không hợp lệ');
        return false;
    } else {
        showMessage(idSpan, '');
        return true
    }
}

function isValidDateFormat(dateString) {
    var pattern = /^\d{2}\/\d{2}\/\d{4}$/;
    return pattern.test(dateString);
}
function checkDate(idSpan, date) {

    if (isValidDateFormat(date)) {
        showMessage(idSpan, "");
        return true;
    }
    else {
        showMessage(idSpan, "Ngày Tháng Năm không hớp lệ");
        return false;
    }
}
function checkPass(idSpan, password) {
    if (/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(\W|_)).{5,}$/.test(password)) {
        showMessage(idSpan, '');
        return true
    }
    else {
        showMessage(idSpan, 'Mật khẩu không hợp lệ');
        return false;
    }
}
function checkNone(idSpan, value) {
    if (value == '') {
        showMessage(idSpan, 'Vui lòng điền giá trị vào');
        return false;
    }
    else {
        showMessage(idSpan, '');
        return true;
    }
}
function checkPosition(idSpan, value) {
    if (value == "Chọn chức vụ") {
        showMessage(idSpan, 'Vui lòng chọn chức vụ');
        return false;
    }
    else {
        showMessage(idSpan, '');
        return true;
    }
}
