function NhanVien(account, name, email, password, workDay, basicSalary, position, time) {
    this.account = account;
    this.name = name;
    this.email = email;
    this.password = password;
    this.workDay = workDay;
    this.basicSalary = basicSalary;
    this.position = position;
    this.time = time;
    this.totalSalary = function () {
        if (this.position == "Sếp")
            return this.basicSalary * 3;
        else if (this.position == "Trưởng phòng")
            return this.basicSalary * 2;
        else if (this.position == "Nhân viên")
            return this.basicSalary;
    };
    this.staff = function () {
        if (time < 160)
            return "Trung bình"
        else if (time < 176)
            return "Khá"
        else if (time < 192)
            return "Giỏi"
        else return "Xuất sắc"
    }

}