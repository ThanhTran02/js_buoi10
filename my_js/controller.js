function renderStaff(dsnv) {
    var contenHTML = "";
    for (let i = 0; i < dsnv.length; i++) {
        var nv = dsnv[i];
        var conten = `<tr>
        <td>${nv.account}</td>
        <td>${nv.name}</td>
        <td>${nv.email}</td>
        <td>${nv.workDay}</td>
        <td>${nv.position}</td>
        <td>${nv.totalSalary()}</td>
        <td>${nv.staff()}</td>
        <td>
        <button onclick ="xoaNhanVien('${nv.account}')" type="button" class="btn btn-outline-primary">Xoá</button>
        <button onclick ="suaNhanVien('${nv.account}')" type="button" class="btn btn-outline-primary" data-toggle="modal"
        data-target="#myModal">Sửa</button>
        </td>
        </tr>`
        contenHTML = contenHTML + conten;
    }
    document.getElementById("tableDanhSach").innerHTML = contenHTML;
}

function xoaNhanVien(id) {
    var index = dsnv.findIndex(function (item) {
        return item.account == id;
    })
    dsnv.splice(index, 1);

}
function getThongTin() {
    var tk = document.getElementById("tknv").value;
    var ten = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;
    var ngaylam = document.getElementById("datepicker").value;
    var luongCB = document.getElementById("luongCB").value * 1;
    var e = document.getElementById("chucvu");
    var chucvu = e.options[e.selectedIndex].text;
    var gioLam = document.getElementById("gioLam").value * 1;
    return new NhanVien(tk, ten, email, password, ngaylam, luongCB, chucvu, gioLam);
}
function showThongTin(nv) {
    document.getElementById("tknv").value = nv.account;
    document.getElementById("name").value = nv.name;
    document.getElementById("email").value = nv.email;
    document.getElementById("password").value = nv.password;
    document.getElementById("datepicker").value = nv.workDay;
    document.getElementById("luongCB").value = nv.basicSalary;
    document.getElementById("chucvu").value = nv.position;
    document.getElementById("gioLam").value = nv.time;
}
function findStaff(staff, dsnv) {
    var tmpDsnv = [];
    for (let i = 0; i < dsnv.length; i++) {
        if (dsnv[i].staff() == staff)
            tmpDsnv.push(dsnv[i]);
    }
    renderStaff(tmpDsnv);
}
function setLocalData(dssv) {
    var datajson = JSON.stringify(dsnv);
    localStorage.setItem("ds", datajson);
}

function validation(nhanvien) {
    // tk
    var isValid = checkNone("tbTKNV", nhanvien.account) && kiemTraDoDai(4, 6, "tbTKNV", "Tài khoản tối đa 4 - 6 ký số", nhanvien.account);

    // ten
    isValid &= checkNone("tbTen", nhanvien.name) && checkName("tbTen", nhanvien.name);

    // email
    isValid &= checkNone("tbEmail", nhanvien.email) && kiemTraEmail("tbEmail", nhanvien.email);
    // matkhau
    isValid &= checkNone("tbMatKhau", nhanvien.password) && checkPass("tbMatKhau", nhanvien.password) && kiemTraDoDai(6, 10, "tbMatKhau", " Mật Khẩu tối đa 6 - 10 ký tự", nhanvien.password);
    // ngaylam
    isValid &= checkNone("tbNgay", nhanvien.workDay) &&
        checkDate("tbNgay", nhanvien.workDay);
    // basicSalary
    isValid &= checkNone("tbLuongCB", nhanvien.basicSalary) && kiemtGiaTtriTrongKhoang(1000000, 20000000, "tbLuongCB", "Lương cơ bản 1 000 000 - 20 000 000", nhanvien.basicSalary);
    // giờ làm
    isValid &= checkNone("tbGiolam", nhanvien.time) && kiemtGiaTtriTrongKhoang(80, 200, "tbGiolam", "Số giờ làm trong tháng 80 - 200 giờ", nhanvien.time);

    isValid &= checkPosition("tbChucVu", nhanvien.position)
    return isValid;
}