var dsnv = [];
var datajson = localStorage.getItem("ds");
if (datajson != null) {
    dsnv = JSON.parse(datajson).map(function (item) {
        return new NhanVien(item.account, item.name, item.email, item.password, item.workDay, item.basicSalary, item.position, item.time)
    })
    renderStaff(dsnv);
}


function themNhanVien() {
    var nhanvien = getThongTin()
    if (validation(nhanvien)) {
        dsnv.push(nhanvien);
        renderStaff(dsnv);
        setLocalData(dsnv);
        document.getElementById("form").reset();
    }
}

function xoaNhanVien(id) {
    var index = dsnv.findIndex(function (item) {
        return item.account == id;
    });
    dsnv.splice(index, 1);
    renderStaff(dsnv);
    setLocalData(dsnv);
};
function suaNhanVien(id) {
    var index = dsnv.findIndex(function (item) {
        return item.account == id;
    });
    showThongTin(dsnv[index]);
};
function searchNV() {
    var loaiNV = document.querySelector("#searchName").value;
    if (loaiNV != "")
        findStaff(loaiNV, dsnv);
    else
        renderStaff(dsnv);

}
function update() {
    var nhanvien = getThongTin();
    if (validation(nhanvien)) {
        xoaNhanVien(nhanvien.account);
        dsnv.push(nhanvien);
        renderStaff(dsnv);
        setLocalData(dsnv);
        document.getElementById("form").reset();
    }
}



